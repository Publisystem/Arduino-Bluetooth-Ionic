import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

// Pages
import { 
  HelloIonicPage,
  ItemDetailsPage,
  ListPage,
  BluetoothConfigPage,
  LedsPage,
  GsmPage,
  AudioPage,
  LuzPage,
  PuertaPage,
  SensoresPage
 } from '../pages/index.pages';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Bluetooth
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { AudiorecorderProvider } from '../providers/audiorecorder/audiorecorder';

import { IonicStorageModule } from "@ionic/storage";

import { MediaCapture } from '@ionic-native/media-capture';
import { Media } from '@ionic-native/media';
import { File } from "@ionic-native/file";
import { ReportProvider } from '../providers/report/report';

// Report
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { firebaseConfig } from '../config/firebase.config';


@NgModule({
  declarations: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    ListPage,
    BluetoothConfigPage,
    LedsPage,
    GsmPage,
    AudioPage,
    LuzPage,
    PuertaPage,
    SensoresPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    ListPage,
    BluetoothConfigPage,
    LedsPage,
    GsmPage,
    AudioPage,
    LuzPage,
    PuertaPage,
    SensoresPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BluetoothSerial,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AudiorecorderProvider,
    MediaCapture,
    Media,
    File,
    ReportProvider
  ]
})
export class AppModule {}
