import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GsmPage } from './gsm';

@NgModule({
  declarations: [
    GsmPage,
  ],
  imports: [
    IonicPageModule.forChild(GsmPage),
  ],
})
export class GsmPageModule {}
