import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BluetoothConfigPage } from './bluetooth-config';

@NgModule({
  declarations: [
    BluetoothConfigPage,
  ],
  imports: [
    IonicPageModule.forChild(BluetoothConfigPage),
  ],
})
export class BluetoothConfigPageModule {}
