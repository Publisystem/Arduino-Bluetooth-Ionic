import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LedsPage } from './leds';

@NgModule({
  declarations: [
    LedsPage,
  ],
  imports: [
    IonicPageModule.forChild(LedsPage),
  ],
})
export class LedsPageModule {}
